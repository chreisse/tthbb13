from TTH.MEAnalysis.MEAnalysis_cfg_heppy import Conf

Conf.mem["calcME"] = True
Conf.mem["methodsToRun"] = [
    "DL_0w2h2t",
    "SL_0w2h2t",
    "SL_1w2h2t",
    "SL_2w2h2t",
]
Conf.mem["enabled_systematics"] = ["nominal"]
Conf.mem["selection"] = lambda event: (
        ((event.is_sl and event.nominal_event.numJets>=4 and event.nominal_event.nBCSVM >= 3) or
        (event.is_dl and event.nominal_event.numJets>=4 and event.nominal_event.nBCSVM>=3))
)
