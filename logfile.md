

Apr5_leptonic_v1
-------------
tthbb13=2b04824
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr12_JECV4_v1
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr12_JECV4_v1
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr12_JECV4_v2
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr13_JECV4
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr13_JECV4
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr14_split
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr15_fixpv
-------------
tthbb13=aa0a19e
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr16_jecsyst
-------------
tthbb13=cbea4bc
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr20_cmssw
-------------
tthbb13=bddd61c
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr20_cmssw
-------------
tthbb13=bddd61c
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=e8a78fb


Apr20_cmssw_v1
-------------
tthbb13=bddd61c
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=8c336cb


Apr20_cmssw_v1
-------------
tthbb13=bddd61c
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=8c336cb


Apr20_cmssw_v2
-------------
tthbb13=8b6d33d
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=63846b5


Apr20_cmssw_v1
-------------
tthbb13=2de6ecf
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=63846b5


Apr20_cmssw_v2
-------------
tthbb13=2de6ecf
CommonClassifier=91f1d2f
MEIntegratorStandalone=4a1ce78
CMSSW=63846b5


Aug3_syst
-------------
tthbb13=3428ab5
CommonClassifier=495d763
MEIntegratorStandalone=4a1ce78
CMSSW=a650315


Aug3_data
-------------
tthbb13=3428ab5
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=a650315


Aug4_slimmed
-------------
tthbb13=3428ab5
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=a650315


Aug5_slimmed
-------------
tthbb13=3428ab5
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=a650315


Aug9_isr
-------------
tthbb13=3428ab5
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=a650315


Aug11_isr
-------------
tthbb13=72f6178
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=3d863ac


Aug11_isr
-------------
tthbb13=72f6178
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=3d863ac


Sep4_v1
-------------
tthbb13=d4929d3
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=c8be8b8


Sep6_v1
-------------
tthbb13=c1d076f
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=ee5da89


Sep8_memcheck
-------------
tthbb13=c1d076f
CommonClassifier=8938f08
MEIntegratorStandalone=4a1ce78
CMSSW=291ac54


Sep20_data
-------------
tthbb13=53e22d6
CommonClassifier=8938f08
MEIntegratorStandalone=9c93488
CMSSW=291ac54


Sep20_data
-------------
tthbb13=1d78c59
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54


Sep26
-------------
tthbb13=5034feb
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54


Sep26
-------------
tthbb13=5034feb
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54


Sep28
-------------
tthbb13=5034feb
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54


Oct16_puid_LHE
-------------
tthbb13=96f3a4d
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54f


Oct16_puid_LHE_v2
-------------
tthbb13=96f3a4d
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=291ac54f


Oct16_puid_LHE_v3
-------------
tthbb13=96f3a4d
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Oct16_puid_LHE_v3
-------------
tthbb13=96f3a4d
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Oct19_memcheck2
-------------
tthbb13=96f3a4d
CommonClassifier=69534ea
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Oct21_tt
-------------
tthbb13=cba59d2
CommonClassifier=a10a9df
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Oct27_data
-------------
tthbb13=a2fa0b8
CommonClassifier=a10a9df
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Nov1_mc
-------------
tthbb13=38d1bda
CommonClassifier=a10a9df
MEIntegratorStandalone=3587b02
CMSSW=050bb6b


Nov30_ttbb
-------------
tthbb13=ccf814d
CommonClassifier=a10a9df
MEIntegratorStandalone=3587b02
CMSSW=f3408dc


Apr30_v1
-------------
tthbb13=895f34c6
CommonClassifier=a10a9df
MEIntegratorStandalone=7566e96
CMSSW=41220f99aff


May1
-------------
tthbb13=4963733a
CommonClassifier=a10a9df
MEIntegratorStandalone=7566e96
CMSSW=41220f99aff


May2
-------------
tthbb13=4963733a
CommonClassifier=a10a9df
MEIntegratorStandalone=7566e96
CMSSW=41220f99aff


May14_v1
-------------
tthbb13=803c4d5f
CommonClassifier=a10a9df
MEIntegratorStandalone=ab31bb6
CMSSW=41220f99aff


ttH_AH_v1
-------------
tthbb13=3ef88200
CommonClassifier=a10a9df
MEIntegratorStandalone=ab31bb6
CMSSW=bba8bc89cf0
